@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">News from TVNET</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @foreach ($items as $item)
                            <div class="item">
                                <div class="categories">
                                    Categories: |
                                    @foreach ($item->get_categories() as $category)
                                        <a href="{{ $category->get_scheme() }}">{{ $category->get_term() }}</a> |
                                    @endforeach
                                </div>
                                <br>
                                <h2><a href="{{ $item->get_permalink() }}">{{ $item->get_title() }}</a></h2>
                                <img src="{{  $item->get_enclosure()->get_link() }}">
                                <p>{{ $item->get_description() }}</p>
                                <p>
                                    <small>Posted on {{ $item->get_date('j F Y | g:i a') }}
                                        by {{ $item->get_author()->get_email() }}</small>
                                </p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
