## Install project

To install and configure an existing project, you'd typically

1. Check out its source code
2. Run composer install
3. Configure .env file
4. Run php artisan migrate


## Configuration of application

For configuration of parser rss from tvnet you can change /app/config/tvNet.php file.

There it is possible to customize the limit of articles displayed on the page 
and a link to the page from which we are parsing the data.
