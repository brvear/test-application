<?php

namespace App\Http\Services\Parser;

interface ParserInterface
{
    /**
     * @return array
     */
    public function getItems(): array;
}
