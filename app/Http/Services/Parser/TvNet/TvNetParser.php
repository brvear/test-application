<?php

namespace App\Http\Services\Parser\TvNet;

use App\Http\Services\Parser\ParserInterface;
use willvincent\Feeds\Facades\FeedsFacade;

class TvNetParser implements ParserInterface
{

    /**
     * Parsing items from resource according to configuration file tvNet.php
     * Method return empty array or array with items with a set limit according to configuration file tvNet.php
     *
     * @return array
     */
    public function getItems(): array
    {
        $feed = FeedsFacade::make(config('tvNet.url'));
        $items = $feed->get_items();
        array_splice($items, config('tvNet.limit'));

        return $items;
    }
}
