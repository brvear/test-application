<?php

namespace App\Http\Controllers;

use App\Http\Services\Parser\ParserInterface;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }


    public function index(ParserInterface $parser)
    {
        $items = $parser->getItems();

        return view('news')->with('items', $items);
    }
}
